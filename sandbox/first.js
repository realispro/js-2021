// *** Primitive Types
var dayOfWeek = 3;
var weekend = dayOfWeek >= 5;
var type = typeof weekend;
console.log("Hello :) weekend:[" + weekend + "]");

// *** Date
var today = new Date();
console.log("date:" + today.getDate() + "/" + (today.getMonth()+1) + "/" + today.getFullYear());

// *** RegExp
var address = "Krakow 30-512, ul. Bracka 6 31-012";
var regExp = /\d\d-?\d\d\d/g; //new RegExp("\\d\\d-?\\d\\d\\d", "g");

var result;
while((result=regExp.exec(address))!==null){
    console.log("found: " + result[0] + " at index: " + result.index);
}

// *** Arrays
var technologies = ["JS", "HTML", "C#", "VB", "Java"];
technologies.push("CSS", "require.js");
console.log("first technology: " + technologies.shift() );
console.log("count:" + technologies.length);

// *** Objects
var propertyName = "budget";
var project = new Object();
project.title = "Important project";
project.technologies = technologies;
project.devs = ["Kasia", "Asia", "Bartek", "Marek"];
project[propertyName] = 1_000_000;

project = {
    title: "Important project",
    technologies: technologies,
    devs: ["Kasia", "Asia", "Bartek", "Marek"],
    budget: 1_000_000
};
project.deadline = new Date(2022, 1, 30);
delete project.budget;
project.title = "The most important project";
console.log(project)

var training = {
    trainer : "A",
    startDate : new Date(2021, 9, 26),
    startString: function () {return this.startDate.toDateString()},
    days : 6,
    trainees : ["B", "C", "D"]
}

for(var propName in training){
    console.log(propName + ": " + training[propName]);
}

var formattedDate = formatDate(training.startDate);
console.log(formattedDate);
console.log("formatted:" + formattedDate.formatted)

function formatDate(date, separator){
    //separator = separator==undefined ? "#" : separator;
    separator = separator || "!"
    return {
        formatted: "" + date.getDate() + separator + (date.getMonth()+1) + separator + date.getFullYear(),
        original: date
    }
}




